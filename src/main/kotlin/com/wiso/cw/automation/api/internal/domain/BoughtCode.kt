package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal
import java.time.ZonedDateTime

data class BoughtCode(
    val codeId: String,
    val codeSoldId: String,
    val codeType: CodeType,
    val value: String,
    val supplierName: String?,
    val supplierPrice: BigDecimal,
    val createdOn: ZonedDateTime,
    val orderId: String,
    val productOrderedId: String,
    val externalOrderId: String?,
)

enum class CodeType{
    CodeText
}