package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal
import java.time.ZonedDateTime

data class Offers (val items: List<Offer>, val page: Int, val total: Int)

data class Offer(
    val id: String,
    val name: String,
    val price: BigDecimal,
    val supplierPrice: BigDecimal,
    val productId: String,
    val accountId: String,
    val supplierKind: SupplierKind,
    val stockKind: StockKind,
    val visibility: OfferVisibility,
    val isRisky: Boolean,
    val isWlIndependent: Boolean,
    val status: OfferStatus,
    val quantity: Int,
    val quantityMix: Int,
    val quantityText: Int,
    val updatedAt: ZonedDateTime,
    val selected: Boolean,
) {
    val offerId = OfferId(id)
}

enum class SupplierKind { Internal, ExternalManual, ExternalAPI }
enum class StockKind { OnStock, Preorder, All, Service }
enum class OfferVisibility { All, API, Platform }
enum class OfferStatus { Active, Inactive, Blocked }

@JvmInline value class OfferId(val id: String)
