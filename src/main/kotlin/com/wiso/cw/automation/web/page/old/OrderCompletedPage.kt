package com.wiso.cw.automation.web.page.old

import com.wiso.cw.automation.web.Driver
import org.openqa.selenium.By

class OrderCompletedPage(driver: Driver) : Page<OrderCompletedPage>(driver, "#/sign-in") {
    private fun orderId() = driver.findElement(By.ByXPath("//*[@id=\"page-wrapper\"]/div[2]/div/div/div/div[3]/div/div[2]"))

    fun getOrderId(): String = orderId().text
}

