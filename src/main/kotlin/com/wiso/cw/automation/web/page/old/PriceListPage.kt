package com.wiso.cw.automation.web.page.old

import com.wiso.cw.automation.web.Driver
import org.openqa.selenium.By.ByXPath
import org.openqa.selenium.By.id

class PriceListPage(driver: Driver) : Page<PriceListPage>(driver, "#/price-list") {
    private fun firstOthersProductQuantityInput() = driver.findElement(ByXPath("/html/body/div/div/div[1]/div[2]/div/div/div/div[3]/table/tbody[2]/tr[2]/td[5]/input"))
    private fun cartLink() = driver.findElement(id("shopping-cart"))
    private fun buyButton() = driver.findElement(ByXPath("//*[@id=\"page-wrapper\"]/div[1]/div/div[2]/div[4]/div[1]/a[3]"))
    private val firstOthersProductAvailabilityBy = ByXPath("/html/body/div/div/div[1]/div[2]/div/div/div/div[3]/table/tbody[2]/tr[2]/td[3]/strong")
    private fun firstOthersProductAvailability() = driver.findElement(firstOthersProductAvailabilityBy)

    fun isFirstOthersProductAvailable(quantity: Int = 1) =
        firstOthersProductAvailabilityBy.isPresent()
                && firstOthersProductAvailability().isDisplayed
                && firstOthersProductAvailability().text.toInt() >= quantity

    fun sendFirstOthersProductQuantityToBuy(quantity: Int): PriceListPage {
        val input = waitForElement(firstOthersProductQuantityInput())
        input.clear()
        input.sendKeys(quantity.toString())
        return this
    }

    fun clickCart(): PriceListPage = waitForElement(cartLink()).click().let { this }
    fun clickBuy(): OrderCompletedPage = waitForElement(buyButton()).click().run { OrderCompletedPage(driver) }
}