package com.wiso.cw.automation.web.page.old

import com.wiso.cw.automation.web.Driver
import org.openqa.selenium.By
import org.openqa.selenium.By.ByXPath
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf
import org.openqa.selenium.support.ui.WebDriverWait


@Suppress("UNCHECKED_CAST")
abstract class Page<T: Page<T>>(protected val driver: Driver, val location: String) {
    protected val wait = WebDriverWait(driver, 3)

    protected fun waitForElement(element: WebElement): WebElement {
        wait.until { element.isDisplayed && element.isEnabled }
        return element
    }
    protected fun priceListLink(): WebElement = driver.findElement(ByXPath("//*[@id=\"scroller-nav\"]/div/div/div/ul[2]/div/li[2]/a/span[2]"))
    protected fun sellProductsLink(): WebElement = driver.findElement(ByXPath("//*[@id=\"scroller-nav\"]/div/div/div/ul[2]/div/li[6]/a/span[2]"))
    protected fun miniOrdersLink(): WebElement = driver.findElement(ByXPath("//*[@id=\"scroller-nav\"]/div/div/div/ul[2]/div/li[14]/a/span[2]"))


    open fun navigate(): T {
        driver.goToV1(location)
        assertPageIsPresent()
        return this as T
    }

    protected fun body(): WebElement = driver.findElement(By.tagName("Body"))

    fun assertPageIsPresent(): T {
        wait.until { driver.currentUrl.split("?")[0].endsWith(location) }
        return this as T
    }

    fun WebElement.send(text: String) {
        wait.until(visibilityOf(this))
        sendKeys(text)
    }

    fun wait(seconds: Int = 1) = Thread.sleep(seconds.toLong()*1000)

    fun By.isPresent() = driver.findElements(this).size > 0


    fun goToPriceListPage(): PriceListPage {
        priceListLink().click()
        return PriceListPage(driver).assertPageIsPresent()
    }

    fun goToSellProductsPage(): SellProductsPage {
        sellProductsLink().click()
        return SellProductsPage(driver).assertPageIsPresent()
    }

    fun goToMiniOrdersPage(): MiniOrdersPage {
        miniOrdersLink().click()
        return MiniOrdersPage(driver).assertPageIsPresent()
    }

}