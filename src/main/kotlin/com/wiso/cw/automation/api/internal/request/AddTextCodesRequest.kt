package com.wiso.cw.automation.api.internal.request

import com.wiso.cw.automation.api.internal.domain.Offer

data class AddTextCodesRequest(
    val codes: List<String>,
    val offerId: String,
    val productId: String,
    val supplierName: String? = null,
) {
    constructor(offer: Offer, codes: List<String>) : this(codes, offer.id, offer.productId)
}
