package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal
import java.time.ZonedDateTime

data class Order(
    val id: String,
    val clientOrderId: String?,
    val identifier: String,
    val totalPrice: BigDecimal,
    val createdDate: ZonedDateTime,
    val accountFullName: String?,
    val accountId: String,
    val status: OrderStatus,
    val productsOrdered: List<ProductOrdered>,
    val orderHistory: List<OrderHistory>,
    val linkedOrders: List<LinkedOrder>,
    val totalQuantity: Int,
    val totalEarnings: BigDecimal?,
    val spreadDiff: BigDecimal?,
    val createdMethod: String,
) {
    val orderId = OrderId(id)
}

data class ProductOrdered(
    val id: String,
    val orderId: String?,
    val orderIdentifier: String?,
    val productId: String,
    val productName: String,
    val productIdentifier: String,
    val quantity: Int,
    val unitPrice: BigDecimal,
    val totalPrice: BigDecimal,
    val toDownloadNumber: Int,
    val downloadedNumber: Int,
    val preOrderNumber: Int,
    val status: String,
    val createdMethod: String?,
)

data class OrderHistory(
    val date: ZonedDateTime,
    val description: String,
    val accountFullName: String?,
)

data class LinkedOrder(
    val identifier: String?,
    val createdDate: ZonedDateTime?,
    val createdBy: String?,
    val status: String?,
    val totalPrice: BigDecimal?,
)

enum class OrderStatus {
    Preorder,
    `To download`,
    Completed,
    Rejected,
    Fulfilling
}

@JvmInline value class OrderId(val id: String) {
    override fun toString(): String = id
}