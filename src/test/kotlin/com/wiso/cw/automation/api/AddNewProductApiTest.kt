package com.wiso.cw.automation.api

import com.wiso.cw.automation.api.internal.client.MiniAdmin
import com.wiso.cw.automation.api.internal.request.AddProductRequest
import io.kotest.matchers.collections.shouldContain

class AddNewProductApiTest(val miniAdmin: MiniAdmin) : InternalApiTest({

    test("add new product") {
        val newProduct = miniAdmin.addNewProduct(AddProductRequest())
        miniAdmin.getAvailableProducts().map { it.id } shouldContain newProduct.id
    }
})