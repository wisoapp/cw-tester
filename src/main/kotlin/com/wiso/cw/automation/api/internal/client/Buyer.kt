package com.wiso.cw.automation.api.internal.client

import com.wiso.cw.automation.api.internal.domain.OfferId
import com.wiso.cw.automation.api.internal.domain.Order
import com.wiso.cw.automation.api.internal.domain.OrderId
import com.wiso.cw.automation.api.internal.domain.ProductId
import com.wiso.cw.automation.api.internal.domain.PurchaseConfirmation
import com.wiso.cw.automation.api.internal.domain.UserOffer
import com.wiso.cw.automation.api.internal.request.AddToCartRequest
import com.wiso.cw.automation.api.internal.request.PurchaseSource
import com.wiso.cw.automation.api.internal.request.PurchaseSource.PRICE_LIST
import org.springframework.web.client.RestTemplate

class Buyer(private val restTemplate: RestTemplate, private val accountIdentifier: String) {

    fun getUserOffers(product: ProductId): Array<UserOffer> =
        restTemplate.getForEntity("/api/offers/userOffers?productId=${product.id}", Array<UserOffer>::class.java).body!!

    fun addToCart(offerId: OfferId, quantity: Int = 1, resourceType: PurchaseSource = PRICE_LIST) =
        restTemplate.postForEntity("/api/shoppingCart",
            AddToCartRequest(offerId.id, quantity, resourceType),
            Void::class.java)

    fun purchase(): PurchaseConfirmation =
        restTemplate.postForEntity("/api/accounts/$accountIdentifier/orders",
            null,
            PurchaseConfirmation::class.java).body!!

    fun getOrder(orderId: OrderId): Order =
        restTemplate.getForEntity("/api/accounts/$accountIdentifier/orders/${orderId.id}", Order::class.java).body!!

    fun makeOrder(offerId: OfferId, quantity: Int = 1, resourceType: PurchaseSource = PRICE_LIST): Order {
        addToCart(offerId, quantity, resourceType)
        val confirmation = purchase()
        return getOrder(confirmation.orderId)
    }

}
