package com.wiso.cw.automation.web.page.old

import com.wiso.cw.automation.web.Driver
import com.wiso.cw.automation.Price
import org.openqa.selenium.By
import org.openqa.selenium.By.ByXPath
import org.openqa.selenium.Keys
import java.util.*

class SellProductsPage(driver: Driver): Page<SellProductsPage>(driver, "/products") {
    private fun currentCodesNumber() = driver.findElement(ByXPath("//*[@id=\"page-wrapper\"]/div[2]/div/div/simple-grid/div/div[2]/div[2]/ul/li[1]/section/div/div/div/div[2]/div/div/div[1]/div[1]/simple-grid/div/div[2]/div[2]/ul/li/div/div[3]/span"))

    private fun firstProductLink() = driver.findElement(ByXPath("//*[@id=\"page-wrapper\"]/div[2]/div/div/simple-grid/div/div[2]/div[2]/ul/li[1]/div/div[1]/a"))
    private val addNewOfferButtonBy = ByXPath("//*[@id=\"page-wrapper\"]/div[2]/div/div/simple-grid/div/div[2]/div[2]/ul/li[1]/section/div/div/div/div[2]/div/div/div[1]/div[2]/button")
    private fun addNewOfferButton() = driver.findElement(addNewOfferButtonBy)
    private fun addNewOfferButtonIsPresent() = driver.findElements(addNewOfferButtonBy).size > 0

    private fun sellPriceInput() = driver.findElement(By.id("price"))
    private fun addOfferButton() = driver.findElement(By.id("btn-add-offer"))

    private fun addTextCodesLink() = driver.findElement(ByXPath("//*[@id=\"page-wrapper\"]/div[2]/div/div/simple-grid/div/div[2]/div[2]/ul/li[1]/section/div/div/div/div[2]/div/div/div[1]/div[1]/simple-grid/div/div[2]/div[2]/ul/li/div/div[4]/div/a[1]"))
    private fun addCodesTextArea() = driver.findElement(ByXPath("//textarea[(@name = 'codes')]"))

    private fun addCodesButton() = driver.findElement(By.id("btn-add-codes-text"))

    fun sellFirstProductTextCodes(codes: List<String> = listOf(UUID.randomUUID().toString()), price: Price = Price(1)): SellProductsPage {

        firstProductLink().click()
        if (addNewOfferButtonIsPresent()) addNewOffer(price)

        val currentCodesNumber = currentCodesNumber().text.toInt()

        waitForElement(addTextCodesLink()).click()
        val codesArea = addCodesTextArea()
        codes.forEach { codesArea.sendKeys(it, Keys.ENTER) }
        addCodesButton().click()
        codesArea.sendKeys(Keys.ESCAPE)

        wait(1)
        val newCodesNumber = currentCodesNumber().text.toInt()
        check(currentCodesNumber + codes.size == newCodesNumber)

        return this
    }

    private fun addNewOffer(price: Price) {
        waitForElement(addNewOfferButton()).click()
        waitForElement(sellPriceInput()).send(price.amount.toString())
        waitForElement(addOfferButton()).click()
    }


}
