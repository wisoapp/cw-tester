import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.5.2"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.5.20"
    kotlin("plugin.spring") version "1.5.20"
}

group = "com.wiso.cw"
version = "1.0.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.3")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.security.oauth:spring-security-oauth2:2.5.1.RELEASE")
    implementation("org.apache.httpcomponents:httpclient:4.5.13")
    implementation("org.springframework.hateoas:spring-hateoas:1.3.2")

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.github.bonigarcia:webdrivermanager:4.4.3")
    implementation("org.hibernate:hibernate-validator:7.0.1.Final")
    implementation("org.seleniumhq.selenium:selenium-java:3.141.59")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation ("io.kotest:kotest-runner-junit5:4.6.0")
    testImplementation("io.kotest:kotest-extensions-spring:4.4.3")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    systemProperties(System.getProperties().mapKeys { it.key as String }) //dzięki temu można nadpisywać Springowe propsy gradlem

    when (System.getProperty("test.profile")) {
        "web" -> include("**/web/**/*Test.class")
        "api" -> include("**/api/*Test.class")
        "all" -> include("**/*Test.class")
        else  -> exclude("**/*Test.class")
    }
}

tasks.bootJar {
    enabled = false
}
