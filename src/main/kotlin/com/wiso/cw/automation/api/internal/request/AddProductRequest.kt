package com.wiso.cw.automation.api.internal.request

import com.wiso.cw.automation.api.internal.domain.ExternalSupplier
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDateTime
import kotlin.random.Random

data class AddProductRequest(
    val name: String = "autotest_" + Random.nextInt().toString(),
    val genreId: Genre = Genre.PREPAID_CARDS_GENRE_ID,
    val identifier: String = name,
    val metaLink: String? = null,
    val coverUrl: String? = null,
    val metaScore: Int? = null,
    val extension: String? = null,
    val description: String? = null,
    val videoLink: String? = null,
    val visible: Boolean = true,
    val steamAppId: String? = null,
    val nameDescription: String? = null,
    val platformDescription: String? = null,
    val languageDescription: String? = null,
    val regionDescription: String? = null,
    val appendCustomDiffValueToRange: Boolean = false,
    val platformId: String = Platform.None.id,
    val regions: List<String> = listOf(),
    val languages: List<String> = listOf(),
    val prices: List<ProductPrices> = listOf(ProductPrices()),
    val priceRangeDifferenceValues: List<ProductPrices> = listOf(),
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val releaseDate: LocalDateTime? = null,
    val producerId: String = Producer.Abacus.id,
    val publisherId: String = Publisher.AcademySoft.id,
    val externalSupplier: ExternalSupplier? = null,
    val externalProductId: String? = null,
)

class ProductPrices(
    val rangeId: String = "0b9763b8-a2ac-46d7-a0e8-6b31a8aaae4f",
    val value: BigDecimal = 100.toBigDecimal(),
    val from: Int? = null,
    val to: Int? = null,
)

enum class Genre{
    ACTION_GENRE_ID,
    ADVENTURE_GENRE_ID,
    ANTIVIRUS_GENRE_ID,
    CASUAL_GAME_GENRE_ID,
    CURRENCIES_GENRE_ID,
    DLC_GENRE_ID,
    FPS_GENRE_ID,
    LIFE_SIMULATION_GENRE_ID,
    MMORPG_GENRE_ID,
    OTHERS_GENRE_ID,
    PREPAID_CARDS_GENRE_ID,
    RACING_GENRE_ID,
    RPG_GENRE_ID,
    RTS_GENRE_ID,
    SIMULATION_GENRE_ID,
    SPORTS_GENRE_ID,
    STRATEGY_SIMULATION_GENRE_ID,
    SUBSCRIPTIONS_GENRE_ID,
    TURN_BASED_STRATEGY_GENRE_ID,
}

enum class Producer(val id: String) {
    Abacus("7ea65fcd-a28a-40b8-9add-96763ddbd86e"),
    Bane_Games("52095466-e1a4-4fcc-b3bd-ad032083b97e"),
    Cave("d1818552-055f-448f-b973-2aaf771bf128"),
    Darksoft("ce2d3566-9164-46aa-836a-581196634dfb"),
}

enum class Publisher(val id: String) {
    AcademySoft("1aa4f5ff-040b-4c3a-94e7-f2bab0e9f13e"),
    Compedia("9ba22f8d-718b-4b54-a2e4-b971be4d7c4d"),
    BlueButton_Games("a93a97e3-c4cb-4228-8730-a7f1633fd6e2"),
    Doublesix("f43f7d3e-6fee-489a-85fd-dac2a93a2bda"),
}

enum class Platform(val id: String) {
    Battle("853cabd4-4207-401d-8423-ccf95d6d0008"),
    Bethesda("67b72e64-27a5-495f-a557-2cffff83f165"),
    Epic_Store("67b72e64-27a5-495f-a567-2cffff83f164"),
    GOG("acff9d5c-820f-11e4-995b-0660ed1d1a4d"),
    iTunes("4cd111f0-fa12-4e88-932a-8e2f3eca0775"),
    None("7edb3cf4-45a4-482a-b0d5-a08371eba978"),
    Official_website("5195cf4b-7bf8-4bb3-af8a-869b2ea2142a"),
    Origin("1f145228-9930-4528-b2ab-1b41013f4dfd"),
    PSN("3a6bf0f0-fc57-406f-a1a9-1f94201fb9cf"),
    Rockstar_Social_Club("47c1d6ee-85ca-11e4-995b-0660ed1d1a4d"),
    Steam("5e4ca873-c8f7-483c-bbcc-ab3dc9df5520"),
    Switch("0e5dd7ce-c92c-11e7-abc4-cec278b6b50a"),
    ubi("5adace98-a988-41c9-b831-1872a8f11a65"),
    Uplay("eea0df86-7238-418f-9a7c-0593e410398b"),
    Windows_Store("05512ab2-25fd-11e8-a676-068fd18d2d2b"),
    Xbox_Live("67b72e64-27a5-495f-a557-2cffff83f164"),
}