package com.wiso.cw.automation.web

import com.wiso.cw.automation.MiniAdmin
import com.wiso.cw.automation.web.page.old.LoginPage
import com.wiso.cw.automation.web.page.old.PriceListPage


class SimplePurchaseTest : AutomationTest({
    this as SimplePurchaseTest

    test("simple purchase") {
        // given login
        val priceListPage = LoginPage(driver).navigate().loginAs(MiniAdmin)

        // when
        sellCodeIfNeeded(priceListPage)

        val orderCompletedPage = priceListPage
            .sendFirstOthersProductQuantityToBuy(1)
            .clickCart()
            .clickBuy()

        val orderId = orderCompletedPage.getOrderId()

        // then order should be present and able to open
        orderCompletedPage
            .goToMiniOrdersPage()
            .openOrder(orderId)
    }
}) {
    private fun sellCodeIfNeeded(page: PriceListPage) {
        if (!page.isFirstOthersProductAvailable()) {
            val sellProductsPage = page.goToSellProductsPage()
            sellProductsPage.sellFirstProductTextCodes().wait()
            sellProductsPage.goToPriceListPage().wait()
        }
    }
}

