package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal

data class UserOffer(
    val offerId: String,
    val price: BigDecimal,
    val quantity: Int,
    val stockKind: StockKind,
) {
    val id = OfferId(offerId)
}
