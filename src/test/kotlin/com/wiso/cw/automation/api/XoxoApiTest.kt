package com.wiso.cw.automation.api

import com.wiso.cw.automation.api.internal.client.Buyer
import com.wiso.cw.automation.api.internal.client.MiniAdmin
import com.wiso.cw.automation.api.internal.domain.ExternalProduct
import com.wiso.cw.automation.api.internal.domain.ExternalSupplier.XOXO
import com.wiso.cw.automation.api.internal.domain.OrderStatus
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe

class XoxoApiTest(val miniAdmin: MiniAdmin, buyer: Buyer) : InternalApiTest({
    val externalProduct = ExternalProduct.ExampleXOXOProduct

    test("import XOXO products") {
        //given
        miniAdmin.schedulerInvoker.invokeXoxoImportProducts()

        // when
        val externalProducts = miniAdmin.getExternalProducts(XOXO)

        // then
        externalProducts.isNotEmpty() shouldBe true
        externalProducts.any { it.externalProductId == externalProduct.id } shouldBe true
    }

    test("purchase from XOXO") {
        // given
        val productId = miniAdmin.getInternalProductIdFor(externalProduct)
        val offer = buyer.getUserOffers(productId).first()
        val quantityToBuy = 2

        // when
        val order = buyer.makeOrder(offer.id, quantityToBuy)
        val codes = miniAdmin.getCodes(order)

        // then
        order.productsOrdered shouldHaveSize 1
        order.productsOrdered.all { it.productId == productId.id } shouldBe true
        order.totalQuantity shouldBe quantityToBuy
        order.status shouldBe OrderStatus.`To download`

        codes.size shouldBe quantityToBuy
        codes.all { it.value.isNotEmpty() } shouldBe true
    }
})