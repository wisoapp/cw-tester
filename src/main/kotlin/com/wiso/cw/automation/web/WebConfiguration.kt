package com.wiso.cw.automation.web

import io.github.bonigarcia.wdm.WebDriverManager
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.validation.annotation.Validated
import java.util.concurrent.TimeUnit.SECONDS
import javax.validation.constraints.NotBlank

@EnableConfigurationProperties(value = [Properties::class])
class WebConfiguration @Autowired constructor(
    private val properties: Properties
) {
    init {
        properties.browser.setupDriver()
    }

    @Bean fun driver() = Driver(properties, webDriver())

    private fun webDriver(): WebDriver =
        properties.browser.driver().apply {
            manage().timeouts().implicitlyWait(5, SECONDS)
            manage().window().maximize()
        }
}

class Driver(private val properties: Properties, webDriver: WebDriver) : WebDriver by webDriver {
    fun goToV1(location: String) = navigate().to("${properties.webV1Url}/$location")
    val jsExecutor = webDriver as JavascriptExecutor
}

@ConfigurationProperties(prefix = "automation")
@Validated
class Properties {
    var browser: Browser = Browser.CHROME
    @NotBlank lateinit var webV1Url: String //for "old" front-end
}

enum class Browser(val driver: () -> WebDriver, private val driverManager: WebDriverManager) {
    CHROME(::ChromeDriver, WebDriverManager.chromedriver()),
    FIREFOX(::FirefoxDriver, WebDriverManager.firefoxdriver());

    fun setupDriver() = driverManager.setup()
}