package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal
import java.time.LocalDateTime

data class ExternalSupplierProduct(
    val id: String,
    val name: String,
    val identifier: String,
    val externalProductId: String,
    val language: String?,
    val externalSupplier: ExternalSupplier,
    val additionalInformation: String?,
    val quantity: Int,
    val imageQty: Int?,
    val sourceCurrency: String,
    val suggestedRetailPrice: BigDecimal?,
    val region: String?,
    val deletedDate: LocalDateTime?,
    val price: ExternalSupplierPrice,
    val internalProductIds: List<ExternalProductAssociation>?,
    val externalSupplierProductPlatform: ExternalSupplierProductPlatform?,
    val publisher: String?,
    val developer: String?,
)

class ExternalSupplierPrice(
    val id: String,
    val externalProductId: String,
    val externalSupplier: ExternalSupplier,
    val sourceCurrency: String,
    val price: BigDecimal,
    val sourcePrice: BigDecimal,
    val suggestedRetailPrice: BigDecimal?,
    val iWantToReceive: BigDecimal?,
    val atReducedPrice: Boolean,
    val updatedOn: LocalDateTime?,

    )
class ExternalProductAssociation
class ExternalSupplierProductPlatform(
    val id: String,
    val name: String?,
    val externalSupplier: ExternalSupplier?,
    val platform: Platform?,
)
class Platform