package com.wiso.cw.automation.api.internal.client

import com.wiso.cw.automation.Price
import com.wiso.cw.automation.api.internal.*
import com.wiso.cw.automation.api.internal.domain.*
import com.wiso.cw.automation.api.internal.request.*
import org.springframework.web.client.RestTemplate

class MiniAdmin(private val restTemplate: RestTemplate) {

    val schedulerInvoker = SchedulerInvoker(restTemplate)

    fun getAvailableProducts(): Array<Product> =
        restTemplate.getForEntity("/api/products/sellManaged?pageSize=100", Array<Product>::class.java).body!!

    fun createNewOffer(product: Product, price: Price, supplierPrice: Price) =
        restTemplate.postForEntity("/api/offers", AddOfferRequest(product, price, supplierPrice), Void::class.java)

    fun getOffers(product: Product): Offers =
        restTemplate.getForEntity("/api/offers?productId=${product.id}", Offers::class.java).body!!

    fun addTextCodes(offer: Offer, codes: List<String>) =
        restTemplate.postForEntity("/api/products/${offer.productId}/codes",
            AddTextCodesRequest(offer, codes),
            Void::class.java)

    fun getExternalProducts(supplier: ExternalSupplier): Array<ExternalSupplierProduct> =
        restTemplate.getForEntity("/api/externalProducts?externalSupplier=$supplier",
            Array<ExternalSupplierProduct>::class.java).body!!

    fun addNewProduct(request: AddProductRequest = AddProductRequest()): ProductId =
        restTemplate.postForObject("/api/products", request, ProductId::class.java)!!

    fun associateProduct(request: AssociateProductRequest) =
        restTemplate.postForEntity(
            "/api/externalProducts/associate?" +
                    "externalSupplier=${request.externalSupplier}" +
                    "&externalProductId=${request.externalProductId}" +
                    "&internalProductIds=${request.internalProductId}" +
                    "&sourceCurrency=${request.sourceCurrency}" +
                    "&visibleInApi=${request.visibleInAPI}" +
                    "&visibleOnPlatform=${request.visibleOnPlatform}",
            null,
            Void::class.java)

    fun addNewProductAssociatedWith(externalProduct: ExternalProduct): ProductId {
        val productId = addNewProduct()
        associateProduct(AssociateProductRequest(
            productId.id,
            externalProduct.supplier,
            externalProduct.id,
            externalProduct.currency))

        return productId
    }

    fun getMatchedProducts(supplier: ExternalSupplier, externalProductName: String): Array<MatchedProducts> =
        restTemplate.getForEntity("/api/matchingProducts/matched" +
                "?externalSupplier=$supplier" +
                "&externalProductName=$externalProductName", Array<MatchedProducts>::class.java).body!!

    fun findInternalProductIdFor(externalProduct: ExternalProduct): ProductId? {
        val matchedProducts = getMatchedProducts(externalProduct.supplier, externalProduct.productName)
        return matchedProducts
            .filter { it.externalProductId == externalProduct.id }
            .find { external -> external.matches.any { it.currency.sourceCurrency == externalProduct.currency } }
            ?.matches
            ?.first()
            ?.productId
    }

    fun getInternalProductIdFor(externalProduct: ExternalProduct) =
        findInternalProductIdFor(externalProduct) ?: addNewProductAssociatedWith(externalProduct)

    fun getCodes(order: Order) =
        restTemplate.getForEntity(
            "/api/orders/${order.id}/productsOrdered/${order.productsOrdered.first().id}/codes",
            Array<BoughtCode>::class.java
        ).body!!
}
