package com.wiso.cw.automation.api.internal.domain

import com.wiso.cw.automation.api.internal.domain.ExternalSupplier.XOXO

enum class ExternalProduct(val supplier: ExternalSupplier, val id: String, val currency: String, val productName: String) {
    ExampleXOXOProduct(XOXO, "50039-500", "ARS", "Adriana Costantini ARG 500 ARS")
}