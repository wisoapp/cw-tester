package com.wiso.cw.automation.api.internal.domain

interface Entity {
    val id: String
}