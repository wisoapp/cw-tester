package com.wiso.cw.automation

import com.wiso.cw.automation.Currency.EURO
import java.math.BigDecimal

data class Price(val amount: BigDecimal, val currency: Currency = EURO) {
    constructor(amount: Int, currency: Currency = EURO) : this(amount.toBigDecimal(), currency)
}

enum class Currency {
    EURO
}
