package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal

data class MatchedProducts(
    val externalProductId: String,
    val name: String,
    val platform: String,
    val region: String?,
    val quantity: Int,
    val externalSupplier: ExternalSupplier,
    val matches: Set<ProductMatch>
)

data class ProductMatch(
    val name: String,
    val internalProductId: String,
    val regions: Set<String>,
    val platform: String,
    val currency: MatchingCurrency
) {
    val productId = ProductId(internalProductId)
}

data class MatchingCurrency(
    val sourceCurrency: String,
    val price: BigDecimal
)