package com.wiso.cw.automation

open class User(
    val username: String,
    val password: String,
)

object MiniAdmin: User("miniadmin_w0@cw.com", "matt")