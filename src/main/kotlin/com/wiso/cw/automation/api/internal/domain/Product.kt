package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal

data class Product(
    val id: String,
    val name: String,
    val stockQuantity: Int,
    val stockValue: BigDecimal,
    val priceGreaterThanOnPriceList: Boolean,
    val status: ProductStatus,
    val profitOrLose: BigDecimal,
    val platform: String,
    val regions: String,
    val languages: String,
)

enum class ProductStatus {
    ACCEPTED, SUGGESTED, DECLINED
}

data class ProductId(val id: String) {
    override fun toString(): String = id
}
