package com.wiso.cw.automation.web

import com.wiso.cw.automation.MiniAdmin
import com.wiso.cw.automation.web.page.old.LoginPage

class LoginTest : AutomationTest({
    this as LoginTest

    test("login") {
        // when login
        val priceListPage = LoginPage(driver).navigate().loginAs(MiniAdmin)

        // then
        priceListPage.assertPageIsPresent()
    }

})

