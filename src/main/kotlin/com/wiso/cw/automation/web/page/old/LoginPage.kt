package com.wiso.cw.automation.web.page.old

import com.wiso.cw.automation.web.Driver
import com.wiso.cw.automation.User
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

class LoginPage(driver: Driver) : Page<LoginPage>(driver, "#/sign-in") {
    private fun emailInput(): WebElement = driver.findElement(By.name("email"))
    private fun passwordInput(): WebElement = driver.findElement(By.name("password"))
    private fun signInButton(): WebElement = driver.findElement(By.xpath("//button[contains(@type, 'submit')]"))

    fun loginAs(user: User): PriceListPage {
        emailInput().send(user.username)
        passwordInput().send(user.password)
        signInButton().click()

        return PriceListPage(driver)
    }
}