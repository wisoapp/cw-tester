package com.wiso.cw.automation.web

import com.wiso.cw.automation.AutomationConfiguration
import io.kotest.core.spec.style.FunSpec
import io.kotest.spring.SpringListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("\${test.env}")
@SpringBootTest(webEnvironment = DEFINED_PORT, classes = [AutomationConfiguration::class, WebConfiguration::class])
abstract class AutomationTest(body: FunSpec.() -> Unit) : FunSpec(body) {
    override fun listeners() = listOf(SpringListener)
    @Autowired lateinit var driver: Driver
}