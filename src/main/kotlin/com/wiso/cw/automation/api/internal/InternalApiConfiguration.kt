package com.wiso.cw.automation.api.internal

import com.wiso.cw.automation.api.internal.client.Admin
import com.wiso.cw.automation.api.internal.client.Buyer
import com.wiso.cw.automation.api.internal.client.ExternalApiPretender
import com.wiso.cw.automation.api.internal.client.MiniAdmin
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails
import org.springframework.security.oauth2.common.AuthenticationScheme
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.DefaultUriTemplateHandler
import java.net.URL

/**
Na razie bazujemy tutaj na starych znanych RestTemplate'ach. Zostały one oznaczone w najnowszym Springu jako przestarzałe i mają być zastąpione przez WebClient.
Jest to opisane tutaj: https://github.com/spring-projects/spring-security/wiki/OAuth-2.0-Migration-Guide
Niestety ten nowy Spring Security nie jest trywialny i na razie zdecydowałem się oddać wersję z RestTemplate a migrację do WebClienta zrobić później.
 */
@Import(InternalApiProperties::class)
class InternalApiConfiguration(
    private val properties: InternalApiProperties,
) {
    @Bean fun externalApiPretender() = ExternalApiPretender(restTemplate(externalApiPretenderCredentials))
    @Bean fun admin() = Admin(restTemplate(credentials(properties.adminUsername, properties.adminPassword)))
    @Bean fun miniadmin() = MiniAdmin(restTemplate(credentials(properties.miniadminUsername, properties.miniadminPassword)))
    @Bean fun buyer() = Buyer(restTemplate(credentials(properties.buyerUsername, properties.buyerPassword)), properties.buyerIdentifier)

    private fun restTemplate(credentials: BaseOAuth2ProtectedResourceDetails): RestTemplate =
        OAuth2RestTemplate(credentials).apply {
            uriTemplateHandler = DefaultUriTemplateHandler().apply { baseUrl = properties.url }
            requestFactory = HttpComponentsClientHttpRequestFactory().apply {
                setConnectTimeout(1000)
                setReadTimeout(60000)
                setConnectionRequestTimeout(1000)
            }
        }

    private fun credentials(clientUsername: String, clientPassword: String) =
        ResourceOwnerPasswordResourceDetails()
            .apply {
                authenticationScheme = AuthenticationScheme.header
                clientAuthenticationScheme = AuthenticationScheme.form
                accessTokenUri = URL("${properties.url}/oauth/token").toString()
                clientId = properties.clientId
                clientSecret = properties.clientSecret
                username = clientUsername
                password = clientPassword
            }

    private val externalApiPretenderCredentials =
        ClientCredentialsResourceDetails().apply {
            authenticationScheme = AuthenticationScheme.header
            clientAuthenticationScheme = AuthenticationScheme.form
            accessTokenUri = URL("${properties.url}/oauth/token").toString()
            clientId = properties.externalApiPretenderId
            clientSecret = properties.externalApiPretenderSecret
        }
}