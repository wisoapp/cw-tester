package com.wiso.cw.automation.web.page.old

import com.wiso.cw.automation.web.Driver
import org.openqa.selenium.By.className

class MiniOrdersPage(driver: Driver): Page<MiniOrdersPage>(driver, "#/whitelabel/orders/mini-orders") {
    private fun orderLinks() = driver.findElements(className("simplegrid-collapse-link"))

    fun openOrder(orderName: String): MiniOrdersPage {
        orderLinks().first { it.text == orderName }.click()
        return this
    }
}