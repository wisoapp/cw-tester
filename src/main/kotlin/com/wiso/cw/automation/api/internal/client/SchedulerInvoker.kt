package com.wiso.cw.automation.api.internal.client

import org.springframework.web.client.RestTemplate

class SchedulerInvoker(private val restTemplate: RestTemplate) {
    fun invokeXoxoImportProducts(): String =
        restTemplate.getForEntity("/api/xoxo/importNewProducts", String::class.java).body!!
}