package com.wiso.cw.automation.api

import com.wiso.cw.automation.api.internal.InternalApiConfiguration
import io.kotest.core.spec.style.FunSpec
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("\${test.env}")
@SpringBootTest(classes = [InternalApiConfiguration::class])
abstract class InternalApiTest(body: FunSpec.() -> Unit = {}): FunSpec(body)