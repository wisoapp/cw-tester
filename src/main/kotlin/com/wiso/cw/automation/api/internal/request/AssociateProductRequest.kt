package com.wiso.cw.automation.api.internal.request

import com.wiso.cw.automation.api.internal.domain.ExternalSupplier

data class AssociateProductRequest(
    val internalProductId: String,
    val externalSupplier: ExternalSupplier,
    val externalProductId: String,
    val sourceCurrency: String,
    val visibleInAPI: Boolean = true,
    val visibleOnPlatform: Boolean = true,
)
