package com.wiso.cw.automation.api.internal.domain

import java.math.BigDecimal

data class PurchaseConfirmation(
    val id: String,
    val balance: BigDecimal,
    val canceledProducts: List<String>?
) {
    val orderId = OrderId(id)
}
