package com.wiso.cw.automation.api.internal.request

import com.wiso.cw.automation.Price
import com.wiso.cw.automation.api.internal.domain.Product
import java.math.BigDecimal

data class AddOfferRequest(
    val isPreorder: Boolean,
    val isWlIndependent: Boolean,
    val name: String,
    val productId: String,
    val price: BigDecimal,
    val supplierPrice: BigDecimal
) {
    constructor(product: Product, price: Price, supplierPrice: Price)
            : this(false, false, "", product.id, price.amount, supplierPrice.amount)
}
