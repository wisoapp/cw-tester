package com.wiso.cw.automation.api

import com.wiso.cw.automation.Price
import com.wiso.cw.automation.api.internal.client.Buyer
import com.wiso.cw.automation.api.internal.client.MiniAdmin
import com.wiso.cw.automation.api.internal.domain.Offer
import com.wiso.cw.automation.api.internal.domain.OrderStatus
import com.wiso.cw.automation.api.internal.domain.Product
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import java.util.UUID

class SimplePurchaseApiTest(val miniAdmin: MiniAdmin, buyer: Buyer) : InternalApiTest({
    this as SimplePurchaseApiTest

    test("simple purchase") {
        // given
        val availableProducts = miniAdmin.getAvailableProducts()
       check(availableProducts.isNotEmpty())

        val product = availableProducts.first()
        val offer = assureOffer(product)
        val quantity = 1

        // when
        val order = buyer.makeOrder(offer.offerId, quantity = quantity)

        // then
        order.productsOrdered shouldHaveSize 1
        order.productsOrdered.first().productId shouldBe product.id
        order.totalQuantity shouldBe quantity
        order.status shouldBe OrderStatus.`To download`
    }
}) {
    fun assureOffer(product: Product): Offer {
        var offers = miniAdmin.getOffers(product)
        if(offers.items.isEmpty()) {
            miniAdmin.createNewOffer(product, Price(1), Price(2))
            offers = miniAdmin.getOffers(product)
        }
        val firstOffer = offers.items.minByOrNull { it.price }!!
        if(firstOffer.quantity == 0)
            miniAdmin.addTextCodes(firstOffer, listOf(UUID.randomUUID().toString()))

        return firstOffer
    }
}