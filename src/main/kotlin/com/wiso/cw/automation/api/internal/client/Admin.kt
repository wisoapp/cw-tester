package com.wiso.cw.automation.api.internal.client

import com.wiso.cw.automation.Price
import com.wiso.cw.automation.api.internal.domain.Product
import com.wiso.cw.automation.api.internal.request.AddOfferRequest
import org.springframework.web.client.RestTemplate

class Admin(private val restTemplate: RestTemplate) {

    fun getAvailableProducts(pageSize: Int = 20) =
        restTemplate.getForEntity("/api/products/sellManaged?pageSize=$pageSize", Array<Product>::class.java).body!!

    fun createNewOffer(product: Product, price: Price, supplierPrice: Price) =
        restTemplate.postForEntity("/api/offers", AddOfferRequest(product, price, supplierPrice), Void::class.java)


}