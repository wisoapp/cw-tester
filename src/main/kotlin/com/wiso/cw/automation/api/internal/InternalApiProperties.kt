package com.wiso.cw.automation.api.internal

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class InternalApiProperties(
    @Value("\${internal.api.url}") val url: String,

    @Value("\${internal.api.externalApiPretender.id}") val externalApiPretenderId: String,
    @Value("\${internal.api.externalApiPretender.secret}") val externalApiPretenderSecret: String,

    @Value("\${internal.api.client.id}") val clientId: String,
    @Value("\${internal.api.client.secret}") val clientSecret: String,

    @Value("\${internal.api.admin.username}") val adminUsername: String,
    @Value("\${internal.api.admin.password}") val adminPassword: String,

    @Value("\${internal.api.miniadmin.username}") val miniadminUsername: String,
    @Value("\${internal.api.miniadmin.password}") val miniadminPassword: String,

    @Value("\${internal.api.buyer.username}") val buyerUsername: String,
    @Value("\${internal.api.buyer.password}") val buyerPassword: String,
    @Value("\${internal.api.buyer.identifier}") val buyerIdentifier: String,
)